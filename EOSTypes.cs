﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;


namespace Empiria.Data.Modeling.InsertProperties {
  class EOSTypes {

    #region Properties
    public int TypeId {
      get;
      set;
    }
    public string TypeName {
      get;
      set;
    }
    public int BaseTypeId {
      get;
      set;
    }
    public string TypeFamily {
      get;
      set;
    }
    public string AssemblyName {
      get;
      set;
    }
    public string ClassName {
      get;
      set;
    }
    public string DisplayName {
      get;
      set;
    }
    public string DisplayPluralName {
      get;
      set;
    }
    public bool FemaleGenre {
      get;
      set;
    }
    public string Documentation {
      get;
      set;
    }
    public string TypeKeywords {
      get;
      set;
    }
    public string SolutionName {
      get;
      set;
    }
    public string SystemName {
      get;
      set;
    }
    public string Version {
      get;
      set;
    }
    public DateTime LastUpdate {
      get;
      set;
    }
    public string TypeDataSource {
      get;
      set;
    }
    public string IdFieldName {
      get;
      set;
    }
    public string NamedIdFieldName {
      get;
      set;
    }
    public string TypeIdFieldName {
      get;
      set;
    }
    public bool IsAbstract {
      get;
      set;
    }
    public bool IsSealed {
      get;
      set;
    }
    public bool IsHistorizable {
      get;
      set;
    }
    public int PostedById {
      get;
      set;
    }
    public DateTime PostingTime {
      get;
      set;
    }
    public char TypeStatus {
      get;
      set;
    }

    #endregion

    #region Public Methdos

    public void Save() {
      SqlConnection sConnection = new SqlConnection("Server=CHRIS-PC\\SQLEXPRESS;Database=Zacatecas;Trusted_Connection=True;");
      string sqlQuery = "INSERT INTO EOSTypes(TypeId,TypeName,BaseTypeId,TypeFamily,AssemblyName,ClassName, " +
                   "DisplayName,DisplayPluralName,FemaleGenre,Documentation,TypeKeywords,SolutionName, " +
                   "SystemName,Version,LastUpdate,TypeDataSource,IdFieldName,NamedIdFieldName,TypeIdFieldName, " +
                   "IsAbstract,IsSealed,IsHistorizable,PostedById,PostingTime,TypeStatus) " +
                   "VALUES (@TypeId,@TypeName,@BaseTypeId,@TypeFamily,@AssemblyName,@ClassName,@DisplayName, " +
                   "@DisplayPluralName,@FemaleGenre,@Documentation,@TypeKeywords,@SolutionName,@SystemName, " +
                   "@Version,@LastUpdate,@TypeDataSource,@IdFieldName,@NamedIdFieldName,@TypeIdFieldName, " +
                   "@IsAbstract,@IsSealed,@IsHistorizable,@PostedById,@PostingTime,@TypeStatus) ";

      SqlCommand sCmd = new SqlCommand(sqlQuery, sConnection);
      sCmd.Parameters.Add(new SqlParameter("@TypeId", SqlDbType.Int)).Value = this.TypeId;
      sCmd.Parameters.Add(new SqlParameter("@TypeName", SqlDbType.VarChar)).Value = this.TypeName;
      sCmd.Parameters.Add(new SqlParameter("@BaseTypeId", SqlDbType.Int)).Value = this.BaseTypeId;
      sCmd.Parameters.Add(new SqlParameter("@TypeFamily", SqlDbType.VarChar)).Value = this.TypeFamily;
      sCmd.Parameters.Add(new SqlParameter("@AssemblyName", SqlDbType.VarChar)).Value = this.AssemblyName;
      sCmd.Parameters.Add(new SqlParameter("@ClassName", SqlDbType.VarChar)).Value = this.ClassName;
      sCmd.Parameters.Add(new SqlParameter("@DisplayName", SqlDbType.VarChar)).Value = this.DisplayName;
      sCmd.Parameters.Add(new SqlParameter("@DisplayPluralName", SqlDbType.VarChar)).Value = this.DisplayPluralName;
      sCmd.Parameters.Add(new SqlParameter("@FemaleGenre", SqlDbType.Bit)).Value = this.FemaleGenre;
      sCmd.Parameters.Add(new SqlParameter("@Documentation", SqlDbType.VarChar)).Value = this.Documentation;
      sCmd.Parameters.Add(new SqlParameter("@TypeKeywords", SqlDbType.VarChar)).Value = this.TypeKeywords;
      sCmd.Parameters.Add(new SqlParameter("@SolutionName", SqlDbType.VarChar)).Value = this.SolutionName;
      sCmd.Parameters.Add(new SqlParameter("@SystemName", SqlDbType.VarChar)).Value = this.SystemName;
      sCmd.Parameters.Add(new SqlParameter("@Version", SqlDbType.VarChar)).Value = this.Version;
      sCmd.Parameters.Add(new SqlParameter("@LastUpdate", SqlDbType.SmallDateTime)).Value = this.LastUpdate;
      sCmd.Parameters.Add(new SqlParameter("@TypeDataSource", SqlDbType.VarChar)).Value = this.TypeDataSource;
      sCmd.Parameters.Add(new SqlParameter("@IdFieldName", SqlDbType.VarChar)).Value = this.IdFieldName;
      sCmd.Parameters.Add(new SqlParameter("@NamedIdFieldName", SqlDbType.VarChar)).Value = this.NamedIdFieldName;
      sCmd.Parameters.Add(new SqlParameter("@TypeIdFieldName", SqlDbType.VarChar)).Value = this.TypeIdFieldName;
      sCmd.Parameters.Add(new SqlParameter("@IsAbstract", SqlDbType.Bit)).Value = this.IsAbstract;
      sCmd.Parameters.Add(new SqlParameter("@IsSealed", SqlDbType.Bit)).Value = this.IsSealed;
      sCmd.Parameters.Add(new SqlParameter("@IsHistorizable", SqlDbType.Bit)).Value = this.IsHistorizable;
      sCmd.Parameters.Add(new SqlParameter("@PostedById", SqlDbType.Int)).Value = this.PostedById;
      sCmd.Parameters.Add(new SqlParameter("@PostingTime", SqlDbType.SmallDateTime)).Value = this.PostingTime;
      sCmd.Parameters.Add(new SqlParameter("@TypeStatus", SqlDbType.Char)).Value = this.TypeStatus;
      sCmd.Connection.Open();
      sCmd.ExecuteNonQuery();
      sCmd.Connection.Close();      
    }

    #endregion Public Methods

  }
}
