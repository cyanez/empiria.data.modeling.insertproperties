﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;


namespace Empiria.Data.Modeling.Properties {
  class DataOperations {
    #region Fields
    private string connectionString = "Server=CHRIS-PC\\SQLEXPRESS;Database=Zacatecas;Trusted_Connection=True;";
    #endregion

    #region Constructors and parsers

    private DataOperations() {
    }

    public static DataOperations Parse() {
      var dOperations = new DataOperations();
      return dOperations;
    }     

    #endregion Constructors and parsers      

    #region Public methods
    
    public DataTable GetEmpiraTypeId(string fieldFilter, string searchFiled){
      SqlConnection connection = new SqlConnection(connectionString);
      DataTable table = new DataTable();
      string sql = "SELECT * " +
                   "FROM EOSTypes " +
                   "WHERE " + fieldFilter + " = ('" + searchFiled + "')   AND (TypeStatus <> 'X') ";
      SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
      adapter.Fill(table);
      return table;
    }  
    
    public DataTable GetTypeRelations(){
      SqlConnection connection = new SqlConnection(connectionString);
      DataTable table = new DataTable();
      string sql = "SELECT * " +
                   "FROM EOSTypeRelations " +
                   "ORDER BY TypeRelationId ";
      SqlDataAdapter adapter = new SqlDataAdapter(sql,connection);
      adapter.Fill(table);
      return table;
    }     


    #endregion Public methods
    
    public void InsertRelation(int typeRelationId,int sourceTypeId,int targetTypeId,string propertyName){
      SqlConnection connection = new SqlConnection(connectionString);
      string sql = "INSERT INTO EOSTypeRelations (TypeRelationId,SourceTypeId,TargetTypeId,AssociationTypeId,RelationTypeFamily,RelationName, " +
                   "DisplayName,DisplayPluralName,Documentation,TypeRelationKeywords,DefaultValue,IsBidirectional, " +
                   "IsSealed,IsRuleBased,IsReadOnly,IsHistorizable,IsKeyword,ProtectionMode,TypeRelationDataSource, " +
                   "SourceIdFieldName,TargetIdFieldName,TypeRelationIdFieldName,Cardinality, AttributeSize, " +
                   "AttributePrecision,AttributeScale,AttributeMinValue,AttributeMaxValue,AttributeDisplayFormat, " +
                   "AttributeIsFixedSize,PostedById, PostingDate,TypeRelationStatus) " +
                   "VALUES(" + typeRelationId + "," + sourceTypeId +"," +	targetTypeId + ",22,'Attribute'," +  
                   "'" + propertyName +"',	'Listo para captura',	'',	'Indica si el acto jurídico está abierto para captura',  " +
                   "'Indica si el acto jurídico está abierto para captura' ,'false',	0,	0,	0,	0,	1,	1,	0, " +
                   "'EOSObjectAttributes',	'ObjectId',	'AttributeValue',	'TypeRelationId',	'',	0,	0,	0,	'','','', " +
                   "0	,-3,	'2007-10-23 00:00:00',	'A') ";
      SqlCommand command = new SqlCommand(sql, connection);
      connection.Open();
      command.ExecuteNonQuery();
      connection.Close();
                                                 
    }      

  }
}
