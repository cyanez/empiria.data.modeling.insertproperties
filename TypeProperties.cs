﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace Empiria.Data.Modeling.Properties {
  class TypeProperties {

    #region Constructors and parsers

    private TypeProperties(string assemblyName) {
      this.AssemblyName = assemblyName;
    }

    public static TypeProperties Parse(string asseblyName) {
      var tool = new TypeProperties(asseblyName);
      return tool;
    }       

    #endregion Constructors and parsers

    #region Properties

    public string AssemblyName{
      get;
      set;
    }

    #endregion

    #region Private Methods

    public List<Type> GetClassByNamepace(Type[] assemblyTypes, string namespaceName) {
      List<Type> classes = new List<Type>();
      foreach (Type assemblyType in assemblyTypes) {
        if ((assemblyType.IsClass) && (assemblyType.Namespace == namespaceName)) {
          classes.Add(assemblyType);
        }
      }
      return classes;
    }

    private int GetLastTypeRelationId(){
      var dataOperation = DataOperations.Parse();
      DataTable typeRelations = dataOperation.GetTypeRelations();
      if (typeRelations.Rows.Count == 0){
        return 1;
      } else {
        return (int) typeRelations.Rows[typeRelations.Rows.Count-1][0];
      }
    }

    private int GetEmpiriaClassTypeId(Type className) {
      var dataOperation = DataOperations.Parse();
      DataTable typesTable = dataOperation.GetEmpiraTypeId("ClassName",className.FullName);      
      if (typesTable.Rows.Count == 0) {
        return 0; //can´t find className in EOSTyes ClassName field
      } else {
        return (int) typesTable.Rows[0][0];
      }                  
    }
       
    private PropertyInfo[] GetProperties(Type className) {
      return  className.GetProperties();    
    } 
    
    private int GetEmpiriaPropertyType(string propertyType){
      var dataOperation = DataOperations.Parse();
      DataTable typesTable = dataOperation.GetEmpiraTypeId("TypeName",propertyType);
      if (typesTable.Rows.Count == 0) {
        return 0;
      } else {
        return (int) typesTable.Rows[0][0];
      }
    }
    
    private void Save(int empClassTypeId, PropertyInfo[] properties){
      int typeRelationId = GetLastTypeRelationId();
      foreach(PropertyInfo property in properties){
        typeRelationId++;
        SaveRelation(typeRelationId, empClassTypeId,property);        
      }
    }

    private void SaveRelation(int typeRelationId, int empClassTypeId, PropertyInfo property) {
      var dataOperation = DataOperations.Parse();
      int empiriaPropertyTypeId  = GetEmpiriaPropertyType(property.PropertyType.Name);
      dataOperation.InsertRelation(typeRelationId, empClassTypeId, empiriaPropertyTypeId, property.Name);
    }

    #endregion Private Methods

    #region Public methods

    public void Execute() {
      Assembly assembly = Assembly.LoadFrom(this.AssemblyName);
      Type[] types = assembly.GetTypes();
      int empClassTypeId = 0;
      List<Type> classList = GetClassByNamepace(types, assembly.GetName().Name);      
      foreach (Type className in classList) {
        empClassTypeId = GetEmpiriaClassTypeId(className);
        PropertyInfo[] properties = GetProperties(className);        
        Save(empClassTypeId, properties);      
      }          
    }  
    #endregion Public methods

  }
}
